import numpy as np
import random
import math
import matplotlib.pyplot as plt

def read_file (file_path):
    file = open(file_path, "r")
    d_array = []
    for line in file:
        d_array.append([float(i) for i in line.rstrip().split(',')])
    dataset = np.array(d_array)
    return dataset

def distance (tgt_point, mean_point):
    return math.sqrt((tgt_point[0] - mean_point[0]) ** 2 + (tgt_point[1] - mean_point[1]) ** 2)

def knn(point, k, stressed, n_stressed):
    stres_dis = []
    n_stres_dis = []
    for data in stressed:
        stres_dis.append(distance(point, data))
    for data in n_stressed:
        n_stres_dis.append(distance(point, data))

    stres_dis.sort();
    n_stres_dis.sort();

    for i in range(k):
        if stres_dis[0] < n_stres_dis[0]:
            stres_dis.pop(0)
        else:
            n_stres_dis.pop(0)

    if len(stres_dis) < len(n_stres_dis):
        return "stressed!"
    else:
        return "not stressed!"

if __name__ == "__main__":
    stressed_dataset = read_file("stressed.csv")
    not_stressed_dataset = read_file("not_stressed.csv")
    # randomize
    random.shuffle(stressed_dataset)
    random.shuffle(not_stressed_dataset)

    train_stressed = stressed_dataset[:len(stressed_dataset) // 2, :]
    train_not_stressed = not_stressed_dataset[:len(not_stressed_dataset) // 2, :]
    test_stressed = stressed_dataset[len(stressed_dataset) // 2: , :]
    test_not_stressed = not_stressed_dataset[len(not_stressed_dataset) // 2: , :]

    error = []

    for k in range(3, 22, 2):
        loss = 0
        for data in test_stressed:
            if knn(data, k, train_stressed, test_not_stressed) == "not_stressed!":
                loss += 1
        for data in test_not_stressed:
            if knn(data, k, train_stressed, test_not_stressed) == "stressed!":
                loss += 1
        error.append([k, loss])

    print(error)
    error = np.transpose(error)
    print(error)


    plt.plot(error[0], error[1])
    plt.xlim(22)
    plt.show()
