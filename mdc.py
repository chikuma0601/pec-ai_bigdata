import matplotlib.pyplot as plt
import random
import math

def read_csv(path, format=str):
    file = open(path, "r")
    result = []
    for line in file:
        result.append([format(data) for data in line.rstrip().split(",") if True])

    return result

def split_data(dataset, train_frac):
    test = []
    train = []
    for data in dataset:
        if random.random() < train_frac:
            train.append(data)
        else:
            test.append(data)
    
    return train, test

def calculate_mean(all_data):
    result = [0, 0]
    for data in all_data:
        result[0] += data[0]
        result[1] += data[1]
    result[0] /= len(all_data)
    result[1] /= len(all_data)
    return result

def distance(mean, test_data):
    return math.sqrt((mean[0] - test_data[0]) ** 2 + (mean[1] - test_data[1]) ** 2)

def predict(test_data, correct_mean, error_mean):
    correct = []
    error = []
    for data in test_data:
        if distance(correct_mean, data) < distance(error_mean, data):
            correct.append(data)
        else:
            error.append(data)
            
    return correct, error

def init_plt(graph_name):
    plt.title(graph_name).set_color("white")
    plt.tick_params(axis="both", color="white")
    plt.ylim(0, 25)
    plt.xlim(0, 15)

def plot_scatter(dataset, args):
    plt.plot([i[0] for i in dataset], [i[1] for i in dataset], args)

def mdc(stress_train, stress_test, not_stress_train, not_stress_test):
    stress_mean = calculate_mean(stress_train)
    not_stress_mean = calculate_mean(not_stress_train)
    
    init_plt("mean points")
    plot_scatter([stress_mean], "rx")
    plot_scatter([not_stress_mean], "bx")
    plt.show()
    
    correct, error = predict(stress_test, stress_mean, not_stress_mean)
    
    init_plt("stressed predict result")
    plot_scatter(correct, "b.")
    plot_scatter(error, "r.")
    plt.show()

    correct, error = predict(not_stress_test, not_stress_mean, stress_mean)
    
    init_plt("not_stressed predict result")
    plot_scatter(correct, "b.")
    plot_scatter(error, "r.")
    plt.show()

if __name__ == '__main__':
    stress_records = read_csv("./datasets/stress_data/stress.csv", format=float)
    not_stress_records = read_csv("./datasets/stress_data/not_stress.csv", format=float)
    
    init_plt("all_data")
    plot_scatter(stress_records, "r.")
    plot_scatter(not_stress_records, "b.")
    plt.show()
    
    stress_train, stress_test = split_data(stress_records, 0.5)
    not_stress_train, not_stress_test = split_data(not_stress_records, 0.5)
    
    init_plt("training data")
    plot_scatter(stress_train, "r.")
    plot_scatter(not_stress_train, "b.")
    plt.show()
    
    init_plt("test_data")
    plot_scatter(stress_test, "r.")
    plot_scatter(not_stress_test, "b.")
    plt.show()

    mdc(stress_train, stress_test, not_stress_train, not_stress_test)
    