import random
import numpy as np

def read_file (file_path):
    file = open(file_path, "r")
    d_array = []
    for line in file:
        d_array.append([float(i) for i in line.rstrip().split(',')])
    dataset = np.array(d_array)
    return dataset

def add_label (data, label):
    labeled_data = []
    for single_data in data:
        labeled_data.append(np.append(single_data, int(label)).tolist())
    return np.array(labeled_data, dtype=np.float)

def dataset_preprocess ():
    stressed_dataset = read_file("stressed.csv")
    not_stressed_dataset = read_file("not_stressed.csv")

    # randomize
    random.shuffle(stressed_dataset)
    random.shuffle(not_stressed_dataset)

    # add label to datasets
    stressed_dataset = add_label(stressed_dataset, 1)
    not_stressed_dataset = add_label(not_stressed_dataset, 0)

    # split the data to training and test data
    train_stressed = stressed_dataset[:len(stressed_dataset) // 2, :]
    train_not_stressed = not_stressed_dataset[:len(not_stressed_dataset) // 2, :]
    test_stressed = stressed_dataset[len(stressed_dataset) // 2: , :]
    test_not_stressed = not_stressed_dataset[len(not_stressed_dataset) // 2: , :]

    # combine the stressed data and not stressed and shuffle it :)
    train_data = np.concatenate([train_stressed, train_not_stressed])
    test_data = np.concatenate([test_stressed, test_not_stressed])
    random.shuffle(train_data)
    random.shuffle(test_data)

    #print(train_data)
    #print(test_data)

    return train_data, test_data

class Neuron:
    def __init__ (self, input_num=2, learning_rate=1) :
        self.weight = np.array([random.random() for i in range(input_num)])
        self.bias = random.random()
        self.learning_rate = learning_rate

    def output_func (self, score):
        if score > 0:
            return 1
        return 0

    def get_score (self, feature):
        return self.output_func(np.dot(feature, self.weight) + self.bias)

    def is_good_enough (self, train_data):
        error = 0
        for data in train_data:
            if data[2] != self.get_score(data[:2]):
                error += 1
        print(error)
        return error < 10

    def loss (self, data):
        return data[2] - self.get_score(data[:2])

    def back_propagation (self, train_data):
        for data in train_data:
            self.weight = np.add(self.weight, self.learning_rate * self.loss(data) * data[:2])
            self.bias += self.loss(data)

    def train(self, train_data):
        step = 0
        while not self.is_good_enough(train_data):
            if step > 100000:
                print("--training did not go well :(\n--Exceeed 100000 steps, stop.")
                return
            step += 1
            if step % 10 == 0:
                print("epoch: {}, \t weight[0]: {} \t {}".format(step, self.weight[0], self.loss))
            self.back_propagation(train_data)
        print("--train complete, with {} epochs.".format(step))

if __name__ == "__main__":
    train_data, test_data = dataset_preprocess()
    neuron = Neuron(learning_rate=0.001)
    neuron.train(train_data)
    print(neuron.weight)
