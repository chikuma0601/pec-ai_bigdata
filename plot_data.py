"""
    plot_data.py
    Created by Neuenmuller, Kevin :)
"""
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def read_file (file_path):
    file = open(file_path, "r")
    d_array = []
    for line in file:
        d_array.append([float(i) for i in line.rstrip().split(',')])
    dataset = np.array(d_array)
    return dataset

def distance (tgt_point, mean_point):
    return math.sqrt((tgt_point[0] - mean_point[0]) ** 2 + (tgt_point[1] - mean_point[1]) ** 2)

if __name__ == '__main__':
    stressed_dataframe = pd.read_csv("stressed.csv", dtype=float)
    not_stressed_dataframe = pd.read_csv("not_stressed.csv", dtype=float)

    # randomize
    stressed_dataframe = stressed_dataframe.reindex(np.random.permutation(stressed_dataframe.index))
    not_stressed_dataframe = not_stressed_dataframe.reindex(np.random.permutation(not_stressed_dataframe.index))

    stressed_test_data = stressed_dataframe.head(50)
    not_stressed_test_data = not_stressed_dataframe.head(50)

    stressed_train_data = stressed_dataframe.tail(50)
    not_stressed_train_data = not_stressed_dataframe.tail(50)

    plt.plot(stressed_train_data.iloc[:,0], stressed_train_data.iloc[:,1], "r.")
    plt.plot(not_stressed_train_data.iloc[:,0], not_stressed_train_data.iloc[:,1], "b.")

    stressed_mean = stressed_train_data.mean()
    not_stressed_mean = not_stressed_train_data.mean()

    T = []
    F = []

    for w, h in zip(stressed_test_data.iloc[:,0], stressed_test_data.iloc[:,1]):
        d_s = distance([float(w), float(h)], stressed_mean)
        d_ns = distance([float(w), float(h)], not_stressed_mean)
        if d_s < d_ns:
            T.append([float(w), float(h)])
        else:
            F.append([float(w), float(h)])

    plt.plot(T[:,0], T[:,1], "g.")
    plt.show()
