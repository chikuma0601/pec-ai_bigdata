def permute(elements, s):
    if s == 1:
        return elements
    result = []
    for element in elements:
        sublist = []
        for other in elements:
            sublist.append(element + other)
        result = result + sublist
    return permute(result, s - 1)

def permutation(characters, start, end):
    print("Original List {}".format(characters))
    if start == end:
        return characters
    results = []
    for element in characters:
        result = list(element)
        print("Result: {}".format(result))
        sub = characters.copy()
        sub.remove(element)
        for i in (permutation(sub, start + 1, end)):
            result.append(i)
    return result



if __name__ == "__main__":
    #print(permute(["a", "b", "c"], 1))
    #print(permute(["a", "b", "c", "e", "f"], 2))
    print(permutation(["a", "b", "c"], 0, 2))
