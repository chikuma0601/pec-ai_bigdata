import pandas as pd

def read_data ():
    answer = pd.read_csv("dataset/answer.csv", names=["question_id", "body", "score", "creation_date"])
    answer.set_index("question_id", inplace=True)
    return answer

if __name__ == "__main__":
    text_score = read_data()
    text_score = text_score.drop(columns=["creation_date"])
    highest_vote_posts = text_score.nlargest(10, "score")
    print(highest_vote_posts)

    for row in highest_vote_posts:
        print(row)
