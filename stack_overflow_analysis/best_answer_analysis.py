from google.cloud import bigquery
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
import _pickle as cPickle

from sklearn.model_selection import train_test_split

# loss function
from sklearn.metrics import mean_squared_error
# from sklearn.metrics import accuracy_score

# classification
from sklearn.neighbors import KNeighborsClassifier

# Clustering
from minisom import MiniSom

# Regression
from sklearn.linear_model import Ridge
# from sklearn.ensemble import RandomForestClassifier
# from sklearn.ensemble import BaggingClassifier
# from sklearn.svm import SVR

def query_stackoverflow ():
    # grabs the question by random, limited to 1000 record of data
    query = """SELECT ID, TITLE, BODY, TAGS, SCORE, CREATION_DATE FROM `pec-ai-atelier.stackoverflow.posts_questions` ORDER BY RAND() LIMIT 100000"""

    return pd.read_gbq(query, project_id = "pec-ai-atelier", dialect = "standard")

def get_correspond_answer(question_id):
    # grabs the question by random, limited to 1000 record of data
    query = """"""
    query += "SELECT PARENT_ID, BODY, SCORE, CREATION_DATE FROM `pec-ai-atelier.stackoverflow.posts_answers` WHERE PARENT_ID IN (" + question_id + ")"

    return pd.read_gbq(query, project_id = "pec-ai-atelier", dialect = "standard")


def preprocessing (question, answer):
    indexs = [i for i, value in enumerate(answer["SCORE"]) if abs(float(value) - answer["SCORE"].mean()) > 3 * answer["SCORE"].std()]
    preprocessed_answer = answer.drop(indexs)
    # preprocessed_answer["score"] += np.array(get_body_length(preprocessed_answer), dtype = float) / 100
    # preprocessed_answer["score"] /= preprocessed_answer["score"].max()

    tags = []
    for _, row in question.iterrows():
        tags.extend(row["TAGS"].split('|'))
    frequent_tags = pd.Series(np.array(tags)).value_counts()[:100]
    # frequent_tags.plot(kind="barh")
    # plt.show()

    dataset = question.assign(title_length = np.array(get_title_length(question)))
    dataset.set_index("ID", inplace = True)
    dataset = dataset.assign(code_sec_count = np.array(get_code_section_count(question)))
    dataset = dataset.assign(question_length = np.array(get_body_length(question)))
    dataset = add_tag(dataset, frequent_tags)
    dataset = add_best_upvotes(dataset, preprocessed_answer)

    dataset = dataset.drop(columns = ["TITLE", "BODY", "TAGS", "SCORE", "CREATION_DATE"])
    # print(dataset.head())

    # shuffle
    dataset = dataset.iloc[np.random.permutation(len(dataset))]
    dataset.to_csv("dataset/preprocessed_data.csv")
    return dataset

def add_best_upvotes(dataset, answer):
    dataset = dataset.assign(ans_score = np.zeros(len(dataset), dtype = np.float32))
    for _, row in answer.iterrows():
        if dataset.at[row["PARENT_ID"], "ans_score"] < row["SCORE"]:
            dataset.at[row["PARENT_ID"], "ans_score"] = row["SCORE"]
    return dataset

def add_tag(dataset, frequent_tags):
    for tag in frequent_tags.keys():
        kwargs = {"tag_" + tag : encode_tag(dataset, tag)}
        dataset = dataset.assign(**kwargs)
    return dataset

def encode_tag(dataset, tag_name):
    return [1 if tag_name in tag.split("|") else 0 for tag in dataset["TAGS"]]

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def get_code_section_count(dataset):
    cs_count = np.zeros(dataset["BODY"].count())
    for i, row in enumerate(dataset["BODY"]):
        cs_count[i] =len([i for i in find_all(row, "<code>")])
    return cs_count

def get_body_length(dataset):
    return [len(i) for i in dataset["BODY"]]

def get_title_length(dataset):
    return [len(i) for i in dataset["TITLE"]]

def process_command():
    parser = argparse.ArgumentParser(prog='Stack Overflow Post Analyser', description=':(')
    parser.add_argument('--file', action="store_true", help='Read dataset from a file')

    return parser.parse_args()

def normalize_row(row):
    norm = math.sqrt(np.dot(row.values, np.transpose(row.values)))
    row /= norm
    return row

def normalize_data(dataframe):
    dataframe = dataframe.apply(normalize_row, axis=1, result_type="broadcast")
    return dataframe

def print_origin_data():
    pass

if __name__ == "__main__":
    import argparse

    dataset = None
    args = process_command()
    if args.file:
        dataset = pd.read_csv("dataset/preprocessed_data.csv", index_col = 0)
    else:
        print("Start loading data...")
        print("##", end="", flush=True)
        questions = query_stackoverflow()
        print("###", end="", flush=True)
        answers = get_correspond_answer(",".join((questions["ID"].values.astype(str))))
        print("###", flush=True)
        print("Start")
        dataset = preprocessing(questions, answers)
    target = dataset["ans_score"]
    dataset = dataset.drop(columns = "ans_score")
    # print(target.describe())
    # print(dataset.head(n=10))
    # for column in dataset.columns:
    #     plot_relation(column, dataset[column], "answer score", target)

    # model = Ridge(alpha = 1.0)                                                            # loss = 46.94110287997299
    # model = RandomForestClassifier(n_estimators=10)                                       # loss = 67.85333251953125
    # model = BaggingClassifier(KNeighborsClassifier(), max_samples=0.5, max_features=0.5)  # loss = 47.5533332824707
    # model = SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)                               # loss = 82.32525889928145
    # model = SVR(kernel='poly', C=100, gamma='auto', degree=3, epsilon=.1, coef0=1)        # no result?
    # model = MLPRegressor(hidden_layer_sizes=(5,),
    #                          activation='relu',
    #                          solver='adam',
    #                          learning_rate='constant',
    #                          max_iter=1000000,
    #                          learning_rate_init=0.001,
    #                          alpha=0.01,
    #                          random_state = 59)
    dataset = normalize_data(dataset)
    print("Normalized data: \n", dataset.head())
    som = MiniSom(6, 6, len(dataset.columns), sigma=0.3, learning_rate = 0.5)
    som.train_random(dataset.values, 1000)
    som_activation_response = som.activation_response(dataset.values)

    # sns.heatmap(som_activation_response)
    # plt.show()

    print(f"som activation response: \n{som_activation_response}");
    quantization = som.quantization(dataset.values)

    data_train, data_test, target_train, target_test = train_test_split(quantization, target, test_size = 0.3, random_state = 10)

    model = Ridge(alpha = 1.0, max_iter = 10000)
    model.fit(data_train, target_train)
    predict = model.predict(data_test)

    # save the model
    cPickle.dump(model, open("finalized_model.sav", "wb"))

    # plt.plot([i for i in range(len(target_test))], target_test, "b.")
    # plt.plot([i for i in range(len(predict))], predict, "r.")
    # plt.show()

    # plt.plot([i for i in range(len(predict))], predict - target_test, "r.")
    # plt.show()

    print(f"{type(model).__name__} accuracy: {mean_squared_error(target_test, predict)}")


    # models = [GaussianNB(), LinearSVC(random_state = 0), KNeighborsClassifier(n_neighbors = 3)]
    # for model in models:
    #     pred = model.fit(data_train, target_train).predict(data_test)
    #     print(f"{type(model).__name__} accuracy: {accuracy_score(target_test, pred, normalize = True)}")

    #     visualizer = ClassificationReport(model, classes = ['Won', 'Loss'])
    #     visualizer.fit(data_train, target_train)

    #     visualizer.score(data_test, target_test)

    #     g = visualizer.poof()
