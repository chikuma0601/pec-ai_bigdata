from google.cloud import bigquery

def query_stackoverflow ():
    client = bigquery.Client()
    # grabs the question by random, limited to 1000 record of data
    query = """SELECT ID, TITLE, BODY, TAGS, SCORE, CREATION_DATE FROM `bigquery-public-data.stackoverflow.posts_questions` ORDER BY RAND() LIMIT 1000"""

    query_job = client.query(query)

    results = query_job.result()  # Waits for job to complete.
    return results

def get_correspond_answer(question_id):
    client = bigquery.Client()
    # grabs the question by random, limited to 1000 record of data
    query = """"""
    query += "SELECT PARENT_ID, BODY, SCORE, CREATION_DATE FROM `bigquery-public-data.stackoverflow.posts_answers` WHERE PARENT_ID IN (" + question_id + ")"

    query_job = client.query(query)

    results = query_job.result()  # Waits for job to complete.
    return results

def format_string (text):
    text = text.replace(",", "[comma]")
    text = text.replace("\r", "")
    return text.replace("\n", "[lf]")

def print_result (results, output_file_name="dataset/question.csv"):
    question_id = ""
    file = open(output_file_name, "w")
    for row in results:
        file.write("{},{},{},{},{},{}\n".format(row.ID, format_string(row.TITLE), format_string(row.BODY), row.TAGS, row.SCORE, row.CREATION_DATE))
        question_id += (str(row.ID) + ",")
    file.close()

    question_id = question_id[:-1]

    answers = get_correspond_answer(question_id)
    file_ans = open("dataset/answer.csv", "w")
    for row in answers:
        file_ans.write("{},{},{},{}\n".format(row.PARENT_ID, format_string(row.BODY), row.SCORE, row.CREATION_DATE))
    file_ans.close()

if __name__ == "__main__":
    print_result(query_stackoverflow())
