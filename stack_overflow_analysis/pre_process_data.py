import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

from sklearn.model_selection import train_test_split

# loss function
from sklearn.metrics import mean_squared_error
# from sklearn.metrics import accuracy_score

# classification
from sklearn.neighbors import KNeighborsClassifier

# Regression
# from sklearn.linear_model import Ridge
# from sklearn.ensemble import RandomForestClassifier
# from sklearn.ensemble import BaggingClassifier
from sklearn.svm import SVR

# String Feature Extracter
from sklearn.feature_extraction.text import CountVectorizer

matplotlib.rcParams['interactive'] == True

def preprocessing ():
    question = pd.read_csv("dataset/question.csv", names=["id", "title", "body", "tags", "score", "creation_date"])
    question_title = question["title"]
    # remove punctuation
    question_title = question_title.str.replace('[^\w\s]','')
    # to lower
    question_title = question_title.apply(lambda x: " ".join(x.lower() for x in x.split()))
    # remove common and rare word
    word_frequency = pd.Series(' '.join(question_title).split()).value_counts()
    mean = word_frequency.mean()
    std = word_frequency.std()
    common_word = ["how", "what", "when", "which", "am", "is", "are", "i", "i'm", "a", "an", "in", "on", "at", "of", "to", "does", "can", "will", "the"]
    common_word += list(word_frequency[:15].index)
    rare_word = list(word_frequency.where(word_frequency < 5).dropna())

    common_and_rare_word = common_word + rare_word
    question_title = question_title.apply(lambda x: " ".join(x for x in x.split() if x not in common_and_rare_word))

    vectorizer = CountVectorizer()
    encoded_feature = vectorizer.fit_transform(question_title.values)
    title_features = pd.DataFrame(encoded_feature.toarray(), columns = vectorizer.get_feature_names())
    question = pd.concat([title_features, question], sort=False)
    print(question.describe())
    plt.show()
    # print(question_title)


#     answer = pd.read_csv("dataset/answer.csv", names=["question_id", "body", "score", "creation_date"])

#     indexs = [i for i, value in enumerate(answer["score"]) if abs(float(value) - answer["score"].mean()) > 3 * answer["score"].std()]
#     preprocessed_answer = answer.drop(indexs)

    dataset = dataset.drop(columns = ["title", "body", "tags", "score", "creation_date"])
    # print(dataset.head())

    # shuffle
    dataset = dataset.iloc[np.random.permutation(len(dataset))]

#     dataset = question.assign(title_length = np.array(get_title_length(question)))
#     dataset.set_index("id", inplace = True)
#     dataset = dataset.assign(code_sec_count = np.array(get_code_section_count(question)))
#     dataset = dataset.assign(question_length = np.array(get_body_length(question)))
#     dataset = add_tag(dataset, frequent_tags)
#     dataset = add_best_upvotes(dataset, preprocessed_answer)

#     dataset = dataset.drop(columns = ["title", "body", "tags", "score", "creation_date"])
#     # print(dataset.head())
# t
#     # shuffle
#     dataset = dataset.iloc[np.random.permutation(len(dataset))]


#     # describe(question_len_score)
#     #plot_data("question body length", question_len_score, 25)

#     plot_relation("# of code in question", dataset["code_sec_count"], "best answer upvotes", dataset["ans_score"])
#     plot_relation("question text length", dataset["question_length"], "best answer upvotes", dataset["ans_score"])
#     plot_relation("length of title", dataset["title_length"], "best answer upvotes", dataset["ans_score"])


    # return dataset

def add_best_upvotes(dataset, answer):
    dataset = dataset.assign(ans_score = np.zeros(len(dataset), dtype = np.float32))
    for _, row in answer.iterrows():
        if dataset.at[row["question_id"], "ans_score"] < row["score"]:
            dataset.at[row["question_id"], "ans_score"] = row["score"]
    return dataset

def add_tag(dataset, frequent_tags):
    for tag in frequent_tags.keys():
        kwargs = {"tag_" + tag : encode_tag(dataset, tag)}
        dataset = dataset.assign(**kwargs)
    return dataset

def encode_tag(dataset, tag_name):
    return [1 if tag_name in tag.split("|") else 0 for tag in dataset["tags"]]

def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub) # use start += 1 to find overlapping matches

def get_code_section_count(dataset):
    cs_count = np.zeros(dataset["body"].count())
    for i, row in enumerate(dataset["body"]):
        cs_count[i] =len([i for i in find_all(row, "<code>")])
    return cs_count

def get_body_length(dataset):
    return [len(i) for i in dataset["body"]]

def get_title_length(dataset):
    return [len(i) for i in dataset["title"]]

def plot_hist(dataset, bin):
    plt.hist(dataset, bins = bin)
    # dataframe.hist(column = column, bins = bin)
    plt.show()

def plot_scatter(dataset):
    plt.plot([i for i in range(len(dataset))], dataset, "b.")
    plt.show()

def plot_data(name, dataset, bin):
    plt.title(name + " (scatter)")
    plt.xlabel("data index")
    plot_scatter(dataset)

    plt.title(name + " (hist)")
    plt.xlabel("batches")
    plot_hist(dataset, bin)

def plot_relation(name1, data1, name2, data2):
    plt.title(name1 + " vs " + name2)
    plt.xlabel(name1)
    plt.ylabel(name2)
    plt.plot(data1, data2, "b.")
    plt.show()

def describe(data):
    print(f"count: {len(data)}")
    print(f"max:   {data.max()}")
    print(f"min:   {data.min()}")
    print(f"mean:  {data.mean()}")
    print(f"std:   {data.std()}")

if __name__ == "__main__":
    dataset = preprocessing()
    # target = dataset["ans_score"]
    # dataset = dataset.drop(columns = "ans_score")
    # data_train, data_test, target_train, target_test = train_test_split(dataset, target, test_size = 0.3, random_state = 10)

    # # model = Ridge(alpha = 1.0)                                                            # loss = 46.94110287997299
    # # model = RandomForestClassifier(n_estimators=10)                                       # loss = 67.85333251953125
    # # model = BaggingClassifier(KNeighborsClassifier(), max_samples=0.5, max_features=0.5)  # 47.5533332824707
    # model = SVR(gamma = 0.01, C = 1.0, epsilon = 0.1)

    # model.fit(data_train, target_train)
    # predict = model.predict(data_test)

    # print(f"{type(model).__name__} accuracy: {mean_squared_error(target_test, predict)}")


    # # models = [GaussianNB(), LinearSVC(random_state = 0), KNeighborsClassifier(n_neighbors = 3)]
    # # for model in models:
    # #     pred = model.fit(data_train, target_train).predict(data_test)
    # #     print(f"{type(model).__name__} accuracy: {accuracy_score(target_test, pred, normalize = True)}")

    # #     visualizer = ClassificationReport(model, classes = ['Won', 'Loss'])
    # #     visualizer.fit(data_train, target_train)

    # #     visualizer.score(data_test, target_test)

    # #     g = visualizer.poof()
