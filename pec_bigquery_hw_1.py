from google.cloud import bigquery

print(bigquery.__version__)

def query_stackoverflow ():
    client = bigquery.Client()

    query = """"""

    for i in range(10):
        year = str(2008 + i)
        query += ("(SELECT '" + year + "' AS year, name, gender, count FROM `pec-ai-atelier.test_01.BN_" + year + "` WHERE gender ='F' LIMIT 100 ) UNION ALL " +
                  "(SELECT '" + year + "' AS year, name, gender, count FROM `pec-ai-atelier.test_01.BN_" + year + "` WHERE gender ='M' LIMIT 100 )")
        if i != 9:
            query += "UNION ALL "

    query_job = client.query(query)

    results = query_job.result()  # Waits for job to complete.
    return results

def print_result (results, output_file_name="BN_2008_2018.txt"):
    file = open(output_file_name, "w")
    for row in results:
        file.write("{},{},{},{}\n".format(row.year, row.name, row.gender, row.count))
    file.close()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Reteieve data from 2008 -> 2017.')
    parser.add_argument('--output', required=True, help="The directory of the output file.")
    args = parser.parse_args()

    print_result(query_stackoverflow(), args.output)


    

