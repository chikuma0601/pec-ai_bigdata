import random
import numpy as np
import math
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams['interactive'] == True

def read_file (file_path):
    file = open(file_path, "r")
    d_array = []
    for line in file:
        d_array.append([float(i) for i in line.rstrip().split(',')])
    dataset = np.array(d_array)
    return dataset

def dataset_preprocess ():
    stressed_dataset = read_file("stressed.csv")
    not_stressed_dataset = read_file("not_stressed.csv")

    # combine the stressed data and not stressed and shuffle it :)
    data = np.concatenate([stressed_dataset, not_stressed_dataset])
    random.shuffle(data)

    return data

class K_Clustering:
    def __init__(self, k):
        self.k = k
        self.means = np.random.rand(k, 2)

    def distance(self, mean, point):
        return math.sqrt((mean[0] - point[0]) ** 2 + (mean[1] - point[1]) ** 2)

    def grouping(self, data):
        group = [ [] for i in range(self.k) ]
        for point in data:
            group_index = 0
            min_distance = self.distance(self.means[0], point)
            for i in range(self.k):
                if self.distance(self.means[i], point) < min_distance:
                    min_distance = self.distance(self.means[i], point)
                    group_index = i
            group[group_index].append(point)
        return group

    def find_mean(self, data):
        if len(data) == 0:
            return None
        mean = [0, 0]
        for point in data:
            mean[0] += point[0]
            mean[1] += point[1]
        mean[0] /= len(data)
        mean[1] /= len(data)
        return mean

    def train(self, data):
        while True:
            groups = self.grouping(data)
            means = []
            for group in groups:
                mean = self.find_mean(group)
                if mean == None:
                    means.append(self.means[groups.index(group)])
                else:
                    means.append(mean)

            means = np.array(means)
            if np.array_equal(self.means, means):
                break;
            else:
                self.means = means

def plot_scatter(dataset, args, marker_size = 5):
    plt.plot([i[0] for i in dataset], [i[1] for i in dataset], args, ms = marker_size)

def scikit_kclustering(k, data):
    from sklearn.cluster import KMeans

    # Number of clusters
    kmeans = KMeans(n_clusters=k)
    # Fitting the input data
    kmeans = kmeans.fit(data)
    # Getting the cluster labels
    # labels = kmeans.predict(data)
    # Centroid values
    # return kmeans.cluster_centers_
    return kmeans

def plot_group(groups):
    args = ["r.", "b.", "g."]
    if len(groups) > 3:
        raise "color not enough"
    for i in range(len(groups)):
        plot_scatter(groups[i], args[i])

def plot_scikit_group(data, labels):
    args = ["r.", "b.", "g."]
    for i in range(len(data)):
        plt.plot(data[i][0], data[i][1], args[labels[i]])

if __name__ == "__main__":
    data = dataset_preprocess()
    #plot_scatter(data, "k.")

    cluster = K_Clustering(2)
    cluster.train(data)
    ski_cluster = scikit_kclustering(2, data)
    print("K = 2, means = {}".format(cluster.means))
    print("K = 2 calculating by sci-kit, means = {}".format(ski_cluster))
    plot_scatter(cluster.grouping(data)[0], "k.")
    plot_scatter(cluster.grouping(data)[1], "g.")
    plot_scatter(cluster.means, "b.")
    plot_scatter(ski_cluster, "r.")

    plt.show()

    #plot_scatter(data, "k.")

    cluster = K_Clustering(3)
    cluster.train(data)
    ski_cluster = scikit_kclustering(3, data)
    print("K = 3, means = {}".format(cluster.means))
    print("K = 3 calculating by sci-kit, means = {}".format(ski_cluster))
    plot_scatter(cluster.means, "b.")
    plot_scatter(ski_cluster, "r.")

    plt.show()
