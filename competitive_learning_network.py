# -*- coding: utf-8 -*-
"""
Created on Wed May 22 20:14:10 2019

@author: NTUTCSIE
"""


import random
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams['interactive'] == True

def normalize(vector):
    length = 0
    for i in vector:
        length += i**2
    length = math.sqrt(length)
    vector = [i / length for i in vector]
    return vector

def arg_max(data):
    winner = 0
    for i in range(len(data)):
        if data[i] > data[winner]:
            winner = i
    return winner

file = open("datasets/Practice05_Data.txt", "r", encoding = "utf-8")

raw = [line.rstrip().split("\t") for line in file]
raw = raw[1:]
raw = [(float(line[1]), float(line[2]), float(line[3]), float(line[4])) for line in raw]

# init weights
weights = [[random.random() for j in range(4)] for i in range(10)]
weights = [normalize(weight) for weight in weights]

# normalize data
normalize_data = [normalize(x) for x in raw]

learning_rate = 0.1
iterations = 1
it = 0
for i in range(iterations):
    it += 1
    for data in normalize_data:
        scores = [np.dot(weight, data) for weight in weights]
        winner = arg_max(scores)
                
        weights[winner] += learning_rate * np.subtract(data, weights[winner])

classes = [arg_max([np.dot(weight, data) for weight in weights]) for data in normalize_data]
type = []
for i in range(5):
    for a_class in classes:
        try:
            type.index(a_class)
        except:
            type.append(a_class)

classes = [type.index(i) for i in classes]
colors = ["b.", "g.", "r.", "c.", "m.", "k."]
print("Numbers of classes = {}".format(len(type)))
for i in range(len(normalize_data[0])):
    for j in range(i + 1, len(normalize_data[0])):
        for t in type:
            plt.plot([0, weights[t][i]], [0, weights[t][j]], colors[i].replace(".", "-"))
        for data in normalize_data:
            plt.plot(data[i], data[j], colors[classes[normalize_data.index(data)]])
        plt.show()