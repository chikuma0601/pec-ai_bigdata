
def list_to_str(record):
    result = ""
    for i in record:
        result += str(i) + ','
    return result[:len(result) - 1] + '\n'

def read_all_files():
    records = []
    for i in range(10):
        filePath = template + str(i + 2008) + ".txt"
        file = open(filePath, "r")
        records.extend(read_file(file, str(i + 2008)))
        file.close()
    return records

def read_file(file, year):
    records = []
    for line in file:
        record = [year]
        record.extend(line.rstrip().split(','))
        record[3] = int(record[3])
        records.append(record)
    return records;

def write_file(file, records):
    for i in range(100):
        if i < len(records):
            file.write(list_to_str(records[i]))

template = "./names/yob"

records = read_all_files()

male = [record for record in records if record[2] == 'M']
female = [record for record in records if record[2] == 'F']
male.sort(key=lambda element: element[2])
female.sort(key=lambda element: element[2])

file = open(template + "2008_2018.txt", "w")
write_file(file, male)
write_file(file, female)
file.close()
