from minisom import MiniSom
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv

if __name__ == "__main__":
    img = plt.imread("dasIstTree.jpg")
    #print(img.shape)
    pixels = np.reshape(img, (img.shape[0] *img.shape[1], 3)) / 255

    #print(pixels)

    som = MiniSom(3, 3, 3, sigma=0.1, learning_rate=0.2)
    som.random_weights_init(pixels)
    random_weights = som.get_weights().copy()
    som.train_random(pixels, 1000)

    #print("activation", som.activation_response(pixels))    # All the neurons are acticvated <--- weights of the trained model

    quantization = som.quantization(pixels)
    print("quantization error", som.quantization_error(pixels))

    clustered_image = quantization.reshape(img.shape).astype(dtype="float32")


    plt.imshow(clustered_image)
    plt.show()
    plt.imshow(som.get_weights())
    plt.show()

    clustered_image *= 255  # converting the image to the corresponding format for openCV
    cv_image = cv.cvtColor(clustered_image, cv.COLOR_RGB2BGR)
    cv.imwrite("output_tree.jpg", cv_image)
