from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from minisom import MiniSom
import numpy as np


def read_file():
    file = open("datasets/Practice05_Data.txt", "r", encoding = "utf-8")

    raw = [line.rstrip().split("\t") for line in file]
    raw = raw[1:]
    raw = [(float(line[1]), float(line[2]), float(line[3]), float(line[4])) for line in raw]
    return raw

def plot_data(means, raw, labels):
    for point in means:
        plt.plot(point[0], point[1], ".")
    for point in raw:
        plt.plot(point[0], point[1], "b.")
    plt.show()

def plot_in_3d(data, labels):
    ax = Axes3D(plt.figure())
    ax.scatter([point[0] for point in data], [point[1] for point in data], [point[3] for point in data], c=labels)
    plt.show()

if __name__ == "__main__":
    data = read_file()

    kmeans = KMeans(n_clusters = 5)
    kmeans = kmeans.fit(data)
    labels = kmeans.predict(data)
    controids = kmeans.cluster_centers_

    #plot_in_3d(data, labels)


    som = MiniSom(6, 6, 4, sigma=0.5, learning_rate=0.05)
    som.train_random(data, 1000)

    result_a = som.activation_response(data)    # the centriods of the prediction
    print(result_a)

    img = plt.imread("u_shall_not_passsss.jpg")
    print(img.shape)
    print(img[0, 0])
    #plt.imshow(img)
    #plt.show()

    pixels = np.reshape(img, (img.shape[0] * img.shape[1], 3))
    print(pixels.shape)

    som = MiniSom(3, 3, 3, sigma=0.1, learning_rate=0.2)
    som.train_random(pixels, 100)
    result = som.activation_response(pixels)
    
    print(result)
    #clustered_img = np.reshape(result, (339, 558, 3))
    #print(clustered_img.shape)
    #plt.imshow(clustered_img)
    #plt.show()
    #plt.plot([value for value in result_a], ".")
    #plt.show()
    #plot_data(data, controids, labels)
