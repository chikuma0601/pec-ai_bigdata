"""
    read_n_write.py

    Created by Neuenmuller, 05/15/2019
"""
from google.cloud import bigquery

DATASET_ID = 'test_01'

def query_stackoverflow ():
    client = bigquery.Client()

    query = """"""
    table_ref = client.dataset(DATASET_ID).table('BN_ALL')
    job_config = bigquery.QueryJobConfig()
    #job_config.use_legacy_sql = True
    job_config.destination = table_ref
    job_config.allow_large_results = True

    for i in range(10):
        year = str(2008 + i)
        query += ("(SELECT '" + year + "' AS year, name, gender, count FROM `pec-ai-atelier.test_01.BN_" + year + "` WHERE gender ='F' LIMIT 100 ) UNION ALL " +
                  "(SELECT '" + year + "' AS year, name, gender, count FROM `pec-ai-atelier.test_01.BN_" + year + "` WHERE gender ='M' LIMIT 100 )")
        if i != 9:
            query += "UNION ALL "

    # Start the query, passing in some configuration to store the result to database
    query_job = client.query(
        query,
        location="asia-east1",
        job_config = job_config,
    )

    results = query_job.result()  # Waits for job to complete.
    print ("Query results loaded to table {}".format(table_ref.path))
    return results

def print_result (results, output_file_name="BN_2008_2018.txt"):
    file = open(output_file_name, "w")
    for row in results:
        file.write("{},{},{},{}\n".format(row.year, row.name, row.gender, row.count))
    file.close()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Reteieve data from 2008 -> 2017.')
    parser.add_argument('--output', required=False, help="The directory of the output file.")
    parser.add_argument('--mode', required=True, help="query/file")
    args = parser.parse_args()

    results = query_stackoverflow()
    if args.mode == "file":
        print_result(results, args.output)
